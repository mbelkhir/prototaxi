package myapps;

import java.util.ArrayList;
import java.util.List;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.regex.Pattern;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.streams.errors.StreamsException;
import org.apache.kafka.streams.kstream.KStream;

public class KafkaSinkFourthNode {

    private static Pattern DATE_PATTERN = Pattern.compile(
            "^(((2000|2400|2800|(19|2[0-9](0[48]|[2468][048]|[13579][26])))-02-29)"
            + "|^(((19|2[0-9])[0-9]{2})-02-(0[1-9]|1[0-9]|2[0-8]))"
            + "|^(((19|2[0-9])[0-9]{2})-(0[13578]|10|12)-(0[1-9]|[12][0-9]|3[01]))"
            + "|^(((19|2[0-9])[0-9]{2})-(0[469]|11)-(0[1-9]|[12][0-9]|30))) ([0-1]\\d|2[0-3]):([0-5]\\d):([0-5]\\d)$");

    public static Properties props = new Properties();

    public UUID id = UUID.fromString("2b12237d-f8fe-40c5-a811-c08ddba2d549-0");
    public String CK = "CK-" + id;
    public static String cluster;
    public String numThreads;

    public boolean isLeader = true;
    public boolean isActive = true;
    public int nbAckReceived = 0;
    public int nbAckExprected = 0;
    public List<String> tableOfNewAddresses = new ArrayList<>();

    /**
     * provisional attributes for testing (using public attributes without
     * getters and setters)
     */
    public float CAPACITY;
    public float RATIO_DESIRED_LOAD;
    public float THRES_TOP;
    public float THRES_DOWN;
    public float currentLoad;
    public float numberCurrentInstances;
    public boolean stateScaling;
    public Float sendRate = 0F;
    public float sendTotal = 0F;

    /**
     *
     * @param id
     */
    public KafkaSinkFourthNode(UUID id) {
        this.id = id;
        this.CK = "CK-" + id;
        this.isLeader = false;
        this.isActive = false;
        this.nbAckReceived = 0;
        this.nbAckExprected = 0;
        this.tableOfNewAddresses = new ArrayList<>();
        this.CAPACITY = 500;
        this.RATIO_DESIRED_LOAD = 0.7f;
        this.THRES_TOP = 0.8f;
        this.THRES_DOWN = 0.6f;
        this.numberCurrentInstances = Float.NaN;
        this.currentLoad = 300f;
        this.stateScaling = false;
        this.cluster = "localhost:9092";
        this.numThreads = "1";
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "streamsKafkaSinkFourthNode");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, cluster);
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.NUM_STREAM_THREADS_CONFIG, numThreads);
        props.put(StreamsConfig.METRICS_RECORDING_LEVEL_CONFIG, "DEBUG");
        props.put(StreamsConfig.BUFFERED_RECORDS_PER_PARTITION_CONFIG, Integer.MAX_VALUE);
        props.put(StreamsConfig.POLL_MS_CONFIG, 10);
        props.put(StreamsConfig.METRICS_SAMPLE_WINDOW_MS_CONFIG, 1000);
    }

    public KafkaSinkFourthNode() {
        this.id = UUID.fromString("2b12237d-f8fe-40c5-a811-c08ddba2d549-0");
        this.CK = "CK-" + id;
        this.isLeader = true;
        this.isActive = true;
        this.nbAckReceived = 0;
        this.nbAckExprected = 0;
        this.tableOfNewAddresses = new ArrayList<>();
        this.CAPACITY = 2;
        this.RATIO_DESIRED_LOAD = 0.7f;
        this.THRES_TOP = 0.8f;
        this.THRES_DOWN = 0.6f;
        this.numberCurrentInstances = 1;
        this.currentLoad = 0f;
        this.stateScaling = false;
        this.cluster = "localhost:9092";
        this.numThreads = "4";
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "streamsKafkaSinkFourthNode");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, cluster);
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.NUM_STREAM_THREADS_CONFIG, numThreads);
        props.put(StreamsConfig.METRICS_RECORDING_LEVEL_CONFIG, "DEBUG");
        props.put(StreamsConfig.BUFFERED_RECORDS_PER_PARTITION_CONFIG, Integer.MAX_VALUE);
        props.put(StreamsConfig.POLL_MS_CONFIG, 10);
        props.put(StreamsConfig.METRICS_SAMPLE_WINDOW_MS_CONFIG, 1000);
    }

    public KafkaSinkFourthNode(String cluster, String numThreads) {
        this.id = UUID.fromString("198633cc-7459-4227-b6af-dbada226f8b9");
        this.CK = "CK-" + id;
        this.isLeader = true;
        this.isActive = true;
        this.nbAckReceived = 0;
        this.nbAckExprected = 0;
        this.tableOfNewAddresses = new ArrayList<>();
        this.CAPACITY = 500;
        this.RATIO_DESIRED_LOAD = 0.7f;
        this.THRES_TOP = 0.8f;
        this.THRES_DOWN = 0.6f;
        this.numberCurrentInstances = 1;
        this.currentLoad = 300f;
        this.stateScaling = false;
        this.cluster = "localhost:9092";
        this.numThreads = "1";
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "streamsKafkaSinkFourthNode");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, cluster);
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.NUM_STREAM_THREADS_CONFIG, numThreads);
        props.put(StreamsConfig.METRICS_RECORDING_LEVEL_CONFIG, "DEBUG");
        props.put(StreamsConfig.BUFFERED_RECORDS_PER_PARTITION_CONFIG, Integer.MAX_VALUE);
        props.put(StreamsConfig.POLL_MS_CONFIG, 10);
        props.put(StreamsConfig.METRICS_SAMPLE_WINDOW_MS_CONFIG, 1000);
    }

    public KafkaSinkFourthNode(String cluster, String numThreads, UUID id) {
        this.id = id;
        this.CK = "CK-" + id;
        this.isLeader = false;
        this.isActive = false;
        this.nbAckReceived = 0;
        this.nbAckExprected = 0;
        this.tableOfNewAddresses = new ArrayList<>();
        this.CAPACITY = 500;
        this.RATIO_DESIRED_LOAD = 0.7f;
        this.THRES_TOP = 0.8f;
        this.THRES_DOWN = 0.6f;
        this.numberCurrentInstances = Float.NaN;
        this.currentLoad = 300f;
        this.stateScaling = false;
        this.cluster = "localhost:9092";
        this.numThreads = "1";
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "streamsKafkaSinkFourthNode");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, cluster);
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.NUM_STREAM_THREADS_CONFIG, numThreads);
        props.put(StreamsConfig.METRICS_RECORDING_LEVEL_CONFIG, "DEBUG");
        props.put(StreamsConfig.BUFFERED_RECORDS_PER_PARTITION_CONFIG, Integer.MAX_VALUE);
        props.put(StreamsConfig.POLL_MS_CONFIG, 10);
        props.put(StreamsConfig.METRICS_SAMPLE_WINDOW_MS_CONFIG, 1000);

    }

    public static void main(String[] args) {

        ArrayList<ArrayList<String>> metricsLatenceTest = new ArrayList<>();
        Protocol protocol = new Protocol();
        KafkaSinkFourthNode node2;
        if (args.length == 0) {
            node2 = new KafkaSinkFourthNode();
        } else if (args.length == 1) {
            node2 = new KafkaSinkFourthNode(UUID.fromString(args[0]));
        } else if (args.length == 2) {
            node2 = new KafkaSinkFourthNode(args[0], args[1]);
        } else {
            node2 = new KafkaSinkFourthNode(args[0], args[1], UUID.fromString(args[2]));
        }
        Repertory repertory = new Repertory();
        /**
         * Producer's configuration for Kafka Stream
         */
        /**
         * Producer's configuration for debugging (CK-input/output)
         */
        Properties configProperties = new Properties();
        configProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, cluster);
        configProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        configProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        Producer prodDeploy = new KafkaProducer<>(configProperties);

        /**
         * Producer's configuration for consumer (scaling algorithm)*
         */
        Properties scalingprops = new Properties();
        scalingprops.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, cluster);
        scalingprops.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        scalingprops.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonPOJOSerializer.class.getName());
        Producer prodScaling = new KafkaProducer<>(scalingprops);

        final StreamsBuilder builder = new StreamsBuilder();
        KStream<String, String> inputData = builder.stream("sink-stream-taxi");
        KStream<String, String> output = inputData.filter((key, value) -> {
            System.out.println("Received Data");
            return true;
        });

        ScalingConsumerThread scalingMsg = new ScalingConsumerThread(repertory, node2, prodScaling, protocol, prodDeploy);
        scalingMsg.start();
//        LocalScaling scaling = new LocalScaling(repertory, node2, prodScaling, prodDeploy);
//        scaling.start();
        final Topology topology = builder.build();
        final KafkaStreams streams = new KafkaStreams(topology, props);
        final CountDownLatch latch = new CountDownLatch(1);
//        MetricsThread metricsThread = new MetricsThread(streams, node2);
//        metricsThread.start();
//        MetricThreadKafka metricThreadKafka = new MetricThreadKafka(streams, node2);
//        metricThreadKafka.start();
//        ProducerMetricsThreads prodMetricThread = new ProducerMetricsThreads(streams, node2);
//        prodMetricThread.start();

        // attach shutdown handler to catch control-c
        Runtime.getRuntime().addShutdownHook(new Thread("streams-shutdown-hook") {
            @Override
            public void run() {
                streams.close();
                latch.countDown();
            }
        });
        try {
            streams.start();

            latch.await();
        } catch (IllegalStateException | InterruptedException | StreamsException e) {
            System.exit(1);
        }
    }

}
