package myapps;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.kafka.streams.KafkaStreams;

/**
 *
 * @author mbelkhir
 */
public class ProducerMetricsThreads extends Thread {
    KafkaStreams streams;
    KafkaSinkFourthNode node;
    List<List<Float>> sendRateMetricsByThread;
    List<Float> aux;
    static ArrayList<ArrayList<Float>> metricsLatenceTest = new ArrayList<ArrayList<Float>>();

    public ProducerMetricsThreads(KafkaStreams streams, KafkaSinkFourthNode node) {
        this.streams = streams;
        this.node = node;
        sendRateMetricsByThread = new ArrayList<>();
    }

    @Override
    public void run() {
        int i = 0;
        float messageT1 = 0;
        int timeToReachOneSec = 0;
        BufferedWriter writer;
        while (true) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(MetricsThread.class.getName()).log(Level.SEVERE, null, ex);
            }
            timeToReachOneSec += 1000;
            if (timeToReachOneSec >= 1000) {
                
                ArrayList<Float> auxTest = new ArrayList<>();
                float aux = node.sendTotal;
                node.sendRate = node.sendTotal - messageT1;
                auxTest.add(node.currentLoad);
                auxTest.add(node.sendRate * node.numberCurrentInstances);
                auxTest.add(node.numberCurrentInstances);
                messageT1 = aux;
                timeToReachOneSec = 0;
                metricsLatenceTest.add(auxTest);
                
            }
            if (i == 180 && node.isLeader) {
                try {
                    writer = new BufferedWriter(new FileWriter("outputMetricsProdTest.py"));
                    writer.write("latence = " + metricsLatenceTest);
                    writer.close();
                } catch (IOException ex) {
                    Logger.getLogger(MetricsThread.class.getName()).log(Level.SEVERE, null, ex);
                }
                System.exit(0);
            }
            i++;
        }
    }

}
