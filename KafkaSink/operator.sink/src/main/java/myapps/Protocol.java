package myapps;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static myapps.MetricsThread.metricsLatenceTest;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

/**
 *
 * @author mbelkhir
 */
public class Protocol {

    BufferedWriter writer;

    public void onReceiptOf(Message msg, Repertory repertory, KafkaSinkFourthNode node2, Producer producer, Producer prodDeploy) {
        switch (msg.getType()) {
            case "DELETION": {
                if (repertory.getPreds().contains(msg.getSrc())) {
                    repertory.getPreds().remove(msg.getSrc());
                } else {
                    repertory.getSuccs().remove(msg.getSrc());
                }
                ProducerRecord<String, Message> rec = new ProducerRecord<>(msg.getSrc(),
                        new Message(null, null, null, "DELETION_ACK", msg.getSrc(), node2.CK));
                producer.send(rec);
                ProducerRecord<String, Message> rec0 = new ProducerRecord<>("CK-test0",
                        new Message(null, null, null, "DELETION_ACK", msg.getSrc(), node2.CK));
                producer.send(rec0);
                break;
            }
            case "DUPLICATION": {
                if (repertory.getSuccs().contains(msg.getSrc())) {
                    msg.getAddrs().forEach((newNode) -> {
                        repertory.getSuccs().add(newNode);
                    });
                } else {
                    msg.getAddrs().forEach((newNode) -> {
                        repertory.getPreds().add(newNode);
                    });
                }
                ProducerRecord<String, Message> rec = new ProducerRecord<>(msg.getSrc(),
                        new Message(null, null, null, "DUPLICATION_ACK", msg.getSrc(), node2.CK));
                producer.send(rec);
                ProducerRecord<String, Message> rec0 = new ProducerRecord<>("CK-test0",
                        new Message(null, null, null, "DUPLICATION_ACK", msg.getSrc(), node2.CK));
                producer.send(rec0);
                break;
            }

            case "DUPLICATION_ACK": {
                node2.nbAckReceived = node2.nbAckReceived + 1;
                if (node2.nbAckReceived == node2.nbAckExprected) {
                    node2.tableOfNewAddresses.stream().map((newSibling) -> {
                        ProducerRecord<String, Message> rec = new ProducerRecord<>(newSibling,
                                new Message(null, repertory.getSuccs(), repertory.getPreds(), "START", newSibling, node2.CK));
                        producer.send(rec);
                        ProducerRecord<String, Message> rec0 = new ProducerRecord<>("CK-test0",
                                new Message(null, repertory.getSuccs(), repertory.getPreds(), "START", newSibling, node2.CK));
                        return rec0;
                    }).forEachOrdered((rec0) -> {
                        producer.send(rec0);
                    });
                    node2.stateScaling = false;
                }
                break;
            }
            case "DELETION_ACK": {
                node2.nbAckReceived = node2.nbAckReceived + 1;
                if (node2.nbAckReceived == node2.nbAckExprected) {
                    ProducerRecord<String, String> recDeploy = new ProducerRecord<>("deploy", node2.id + " toDelete");
                    prodDeploy.send(recDeploy);
                    ProducerRecord<String, Message> rec = new ProducerRecord<>("CK-test0",
                            new Message(null, null, null, "DELETED-DESTROYED", node2.CK, node2.CK));
                    producer.send(rec);
                    try {
                        writer = new BufferedWriter(new FileWriter("outputMetricsProdTest" + node2.id.toString().substring(0, 4) + ".py"));
                        writer.write("latence = " + metricsLatenceTest);
                        writer.close();
                        System.out.println(metricsLatenceTest);
                    } catch (IOException ex) {
                        Logger.getLogger(MetricsThread.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    try {
                        writer = new BufferedWriter(new FileWriter("KafkaOutputMetricsProdTest" + node2.id.toString().substring(0, 4) + ".py"));
                        writer.write("latence = " + myapps.MetricThreadKafka.metricsLatenceTest);
                        writer.close();
                        System.out.println(myapps.MetricThreadKafka.metricsLatenceTest);
                    } catch (IOException ex) {
                        Logger.getLogger(MetricsThread.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    System.exit(0);
                }
                break;
            }
            case "START": {
                node2.isActive = true;
                repertory.setPreds(msg.getPreds());
                repertory.setSuccs(msg.getSuccs());
                ProducerRecord<String, Message> rec = new ProducerRecord<>("CK-test0",
                        new Message(null, null, null, "STARTED", node2.CK, node2.CK));
                producer.send(rec);
                break;
            }
            case "METRICS": {
                node2.numberCurrentInstances = Float.parseFloat(msg.getDest());
                node2.currentLoad = Float.parseFloat(msg.getSrc()) * repertory.getPreds().size();
                break;
            }
            default:
                ProducerRecord<String, Message> rec = new ProducerRecord<>("CK-test0",
                        new Message(null, null, null, "ERROR", node2.CK, msg.getType()));
                producer.send(rec);
                break;
        }
    }
}
