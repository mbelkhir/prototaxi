package myapps;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author mbelkhir
 */
public class Repertory {

    private List<String> succs = new ArrayList<>();
    private List<String> preds = new ArrayList<>(Arrays.asList("CK-a6b701ca-9c36-449c-bafe-de014da3e5ea"));

    public List<String> getSuccs() {
        return succs;
    }

    public void setSuccs(List<String> succs) {
        this.succs = new ArrayList<>(succs);
    }

    public List<String> getPreds() {
        return preds;
    }

    public void setPreds(List<String> preds) {
        this.preds = new ArrayList<>(preds);
    }
}
