package myapps;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

/**
 *
 * @author mbelkhir
 */
public class LocalScaling extends Thread {

    Repertory repertory;
    KafkaParserSecondNode node2;
    Producer producer;
    Producer prodDeploy;

    public LocalScaling(Repertory repertory, KafkaParserSecondNode node2, Producer producer, Producer prodDeploy) {
        this.repertory = repertory;
        this.node2 = node2;
        this.producer = producer;
        this.prodDeploy = prodDeploy;
    }

    private float getProbability(Float currentLoad, Float numberCurrentInstances) {

        float globalLoad = currentLoad * numberCurrentInstances;
        float idealLoad = node2.CAPACITY * node2.RATIO_DESIRED_LOAD;
        float nbrInstancesDiff = Math.abs(((float) globalLoad / idealLoad) - numberCurrentInstances);
        return Math.abs(nbrInstancesDiff / (float) numberCurrentInstances);
    }

    private int applyProba(float p) {
        return Math.random() < p ? 1 : 0;
    }

    private void autoScaling(Float currentLoad, Float numberCurrentInstances) {
        System.out.println("currentLoad = " + currentLoad + " numberSiblings = " + numberCurrentInstances);
        if ((currentLoad / node2.CAPACITY >= node2.THRES_TOP) && node2.isActive) {
            scalingOut(currentLoad, numberCurrentInstances);
        } else if ((currentLoad / node2.CAPACITY <= node2.THRES_DOWN && !node2.isLeader && node2.isActive)) {
            scalingIn(currentLoad, numberCurrentInstances);
        }
    }

    private void scalingOut(Float currentLoad, Float numberCurrentInstances) {
        node2.tableOfNewAddresses = new ArrayList<>();
        float p = getProbability(currentLoad, numberCurrentInstances);
        node2.nbAckReceived = 0;
        node2.nbAckExprected = repertory.getPreds().size() + repertory.getSuccs().size();
        int nbrsNewNodes = (int) p + applyProba(p - (int) p);
        if (nbrsNewNodes != 0) {
            node2.stateScaling = true;
            for (int i = 0; i < nbrsNewNodes; i++) {
                UUID newId = java.util.UUID.randomUUID();
                node2.tableOfNewAddresses.add("CK-" + newId);
                //                    Process process = Runtime.getRuntime().exec("kafka-topics.sh --create --zookeeper localhost:2181"
//                            + " --replication-factor 1 --partitions 1 --topic CK-" + newId);
//                    process.waitFor();
                ProducerRecord<String, String> rec = new ProducerRecord<>("deploy", node2.id + " toActive");
                prodDeploy.send(rec);
//                    ProcessBuilder builder = new ProcessBuilder(
//                            "java", "-cp",
//                            "/home/mbelkhir/Documents/DStream/streams.examples/target/streams.examples-0.1.jar"
//                            + ":/home/mbelkhir/Documents/DStream/streams.examples/target/dependency/*", "myapps.KafkaParserSecondNode",
//                            newId.toString());
//                    builder.redirectOutput(
//                            ProcessBuilder.Redirect.to(new File("outputKafkaParserSecondNode-" + newId.toString())));
//                    builder.start();
            }
            repertory.getSuccs().stream().map((succ) -> {
                ProducerRecord<String, Message> rec = new ProducerRecord<>(succ,
                        new Message(node2.tableOfNewAddresses, null, null, "DUPLICATION",
                                succ, node2.CK));
                producer.send(rec);
                ProducerRecord<String, Message> rec0 = new ProducerRecord<>("CK-test0",
                        new Message(node2.tableOfNewAddresses, null, null, "DUPLICATION",
                                succ, node2.CK));
                return rec0;
            }).forEachOrdered((rec0) -> {
                producer.send(rec0);
            });
            repertory.getPreds().stream().map((pred) -> {
                ProducerRecord<String, Message> rec = new ProducerRecord<>(pred,
                        new Message(node2.tableOfNewAddresses, null, null, "DUPLICATION",
                                pred, node2.CK));
                producer.send(rec);
                ProducerRecord<String, Message> rec0 = new ProducerRecord<>("CK-test0",
                        new Message(node2.tableOfNewAddresses, null, null, "DUPLICATION",
                                pred, node2.CK));
                return rec0;
            }).forEachOrdered((rec0) -> {
                producer.send(rec0);
            });
        }
    }

    private void scalingIn(Float currentLoad, Float numberCurrentInstances) {
        node2.nbAckReceived = 0;
        node2.nbAckExprected = repertory.getPreds().size() + repertory.getSuccs().size();
        if (applyProba(getProbability(currentLoad, numberCurrentInstances)) == 1) {
            node2.stateScaling = true;
            repertory.getSuccs().stream().map((succ) -> {
                ProducerRecord<String, Message> rec = new ProducerRecord<>(succ, new Message(null, null, null, "DELETION",
                        succ, node2.CK));
                producer.send(rec);
                ProducerRecord<String, Message> rec0 = new ProducerRecord<>("CK-test0", new Message(null, null, null, "DELETION",
                        succ, node2.CK));
                return rec0;
            }).forEachOrdered((rec0) -> {
                producer.send(rec0);
            });
            repertory.getPreds().stream().map((pred) -> {
                ProducerRecord<String, Message> rec = new ProducerRecord<>(pred, new Message(null, null, null, "DELETION",
                        pred, node2.CK));
                producer.send(rec);
                ProducerRecord<String, Message> rec0 = new ProducerRecord<>("CK-test0", new Message(null, null, null, "DELETION",
                        pred, node2.CK));
                return rec0;
            }).forEachOrdered((rec0) -> {
                producer.send(rec0);
            });
        }
    }

    @Override
    public void run() {
        try {
            Thread.sleep(60000);
        } catch (InterruptedException ex) {
        }
        while (true) {
            Float currentLoad;
            Float numberCurrentInstances;
//            File file = new File("workload.dat");
//            BufferedReader br;
//            try {
//                br = new BufferedReader(new FileReader(file));
//                String st;
//                while ((st = br.readLine()) != null) {
//                    node2.currentLoad = Float.parseFloat(st);
//                }
//            } catch (FileNotFoundException ex) {
//            } catch (IOException ex) {
//            }

            /**
             * Period of scale equals to 10 seconds
             */
            try {
                Thread.sleep(20000);
            } catch (InterruptedException ex) {
            }
            if (!node2.stateScaling) {
                numberCurrentInstances = node2.numberCurrentInstances;
                currentLoad =node2.currentLoad / numberCurrentInstances;
                autoScaling(currentLoad, numberCurrentInstances);
            }
        }
    }
}
