package myapps;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.Producer;

/**
 *
 * @author mbelkhir
 */
public class ScalingConsumerThread extends Thread {

    Properties props = new Properties();
    Protocol protocol;
    Repertory repertory;
    KafkaParserSecondNode node2;
    Producer producer;
    KafkaConsumer<String, Message> consumer;
    Producer prodDeploy;

    public ScalingConsumerThread(Repertory repertory, KafkaParserSecondNode node2, Producer producer, Protocol protocol, Producer prodDeploy) {
        this.repertory = repertory;
        this.node2 = node2;
        this.producer = producer;
        this.prodDeploy = prodDeploy;
        this.protocol = protocol;
        props.put("bootstrap.servers", "localhost:9092");
        props.put("group.id", "consumer" + node2.id);
        props.put("value.deserializer", "myapps.MessageDeserializer");
        props.put("enable.auto.commit", "true");
        props.put("auto.commit.interval.ms", "1000");
        props.put("auto.offset.reset", "earliest");
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Arrays.asList(node2.CK));
    }

    @Override
    public void run() {
        while (true) {
            ConsumerRecords<String, Message> records = consumer.poll(Duration.ofMillis(100));
            for (ConsumerRecord<String, Message> record : records) {
                protocol.onReceiptOf(record.value(), repertory, node2, producer,prodDeploy);
            }
        }
    }
}
