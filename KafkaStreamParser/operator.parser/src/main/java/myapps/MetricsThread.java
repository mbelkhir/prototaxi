package myapps;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.kafka.common.Metric;
import org.apache.kafka.common.MetricName;
import org.apache.kafka.streams.KafkaStreams;

/**
 *
 * @author mbelkhir
 */
public class MetricsThread extends Thread {

    KafkaStreams streams;
    KafkaParserSecondNode node;
    List<List<Float>> sendRateMetricsByThread;
    List<Float> aux;
    static ArrayList<ArrayList<String>> metricsLatenceTest = new ArrayList<ArrayList<String>>();
    List<Float> arrayMetric;

    public MetricsThread(KafkaStreams streams, KafkaParserSecondNode node) {
        this.streams = streams;
        this.node = node;
        sendRateMetricsByThread = new ArrayList<>();
    }

    @Override
    public void run() {

        int i = 0;
        float messageT1 = 0;
        int timeToReachOneSec = 0;
        BufferedWriter writer;
        while (true) {
            long t1 = System.currentTimeMillis();
           
            aux = new ArrayList<>();
            arrayMetric = new ArrayList<>();
            Map<MetricName, ? extends Metric> metrics = streams.metrics();
            metrics.forEach((clientid, metricMap) -> {
                if (metricMap.metricName().name().contains("record-send-total") && metricMap.metricName().toString().contains("topic=output-stream")) {
                    String metricName = metricMap.metricName().toString();
                    String metricValue = metricMap.metricValue().toString();
                    //node.sendTotal = Float.valueOf(metricValue);
                    arrayMetric.add(Float.parseFloat(metricValue));
                }
            });
            node.sendTotal = getSumMetric((ArrayList<Float>) arrayMetric);
            try {
                Thread.sleep(5000);
            } catch (InterruptedException ex) {
                Logger.getLogger(MetricsThread.class.getName()).log(Level.SEVERE, null, ex);
            }
            timeToReachOneSec += 5000;
            if (timeToReachOneSec >= 5000) {
                long t2 = System.currentTimeMillis();
                ArrayList<String> auxTest = new ArrayList<>();
                float aux = node.sendTotal;
                node.sendRate = (node.sendTotal - messageT1)/((t2-t1)/1000);
                auxTest.add(System.currentTimeMillis()+"");
                auxTest.add(node.currentLoad+"");
                auxTest.add(node.sendRate * node.numberCurrentInstances+"");
                auxTest.add(node.numberCurrentInstances+"");
                System.out.println(node.sendRate);
                messageT1 = aux;
                timeToReachOneSec = 0;
                metricsLatenceTest.add(auxTest);
            }
            if (i == 2 && node.isLeader) {
                try {
                    writer = new BufferedWriter(new FileWriter("outputMetricsProdTest"+node.id.toString().substring(0, 4)+".py"));
                    writer.write("latence = " + metricsLatenceTest);
                    writer.close();
                    System.out.println(metricsLatenceTest);
                } catch (IOException ex) {
                    Logger.getLogger(MetricsThread.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            i++;
        }
    }

    public Float getSumMetric(ArrayList<Float> array) {
        Float result = 0F;
        for (Float f : array) {
            result = result + f;
        }
        return result;
    }
}
