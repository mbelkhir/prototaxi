package myapps;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author mbelkhir
 */
public class Repertory {

    private List<String> succs = new ArrayList<>(Arrays.asList("CK-a6b701ca-9c36-449c-bafe-de014da3e5ea"));
    private List<String> preds = new ArrayList<>(Arrays.asList("CK-93f38f9a-e399-4873-8cd2-53b4c5157961"));

    public List<String> getSuccs() {
        return succs;
    }

    public void setSuccs(List<String> succs) {
        this.succs = new ArrayList<>(succs);
    }

    public List<String> getPreds() {
        return preds;
    }

    public void setPreds(List<String> preds) {
        this.preds = new ArrayList<>(preds);
    }
}
