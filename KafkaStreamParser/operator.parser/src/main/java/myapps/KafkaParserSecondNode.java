package myapps;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.regex.Pattern;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.streams.errors.StreamsException;
import org.apache.kafka.streams.kstream.KStream;

public class KafkaParserSecondNode {

    private static Pattern DATE_PATTERN = Pattern.compile(
            "^(((2000|2400|2800|(19|2[0-9](0[48]|[2468][048]|[13579][26])))-02-29)"
            + "|^(((19|2[0-9])[0-9]{2})-02-(0[1-9]|1[0-9]|2[0-8]))"
            + "|^(((19|2[0-9])[0-9]{2})-(0[13578]|10|12)-(0[1-9]|[12][0-9]|3[01]))"
            + "|^(((19|2[0-9])[0-9]{2})-(0[469]|11)-(0[1-9]|[12][0-9]|30))) ([0-1]\\d|2[0-3]):([0-5]\\d):([0-5]\\d)$");

    public static Properties props = new Properties();

    public UUID id = UUID.fromString("198633cc-7459-4227-b6af-dbada226f8b9");
    public String CK = "CK-" + id;
    public static String cluster;
    public String numThreads;

    public boolean isLeader = true;
    public boolean isActive = true;
    public int nbAckReceived = 0;
    public int nbAckExprected = 0;
    public List<String> tableOfNewAddresses = new ArrayList<>();

    /**
     * provisional attributes for testing (using public attributes without
     * getters and setters)
     */
    public float CAPACITY;
    public float RATIO_DESIRED_LOAD;
    public float THRES_TOP;
    public float THRES_DOWN;
    public float currentLoad;
    public float numberCurrentInstances;
    public boolean stateScaling;
    public Float sendRate = 0F;
    public float sendTotal = 0F;

    /**
     *
     * @param id
     */
    public KafkaParserSecondNode(UUID id) {
        this.id = id;
        this.CK = "CK-" + id;
        this.isLeader = false;
        this.isActive = false;
        this.nbAckReceived = 0;
        this.nbAckExprected = 0;
        this.tableOfNewAddresses = new ArrayList<>();
        this.CAPACITY = 500;
        this.RATIO_DESIRED_LOAD = 0.7f;
        this.THRES_TOP = 0.8f;
        this.THRES_DOWN = 0.6f;
        this.numberCurrentInstances = Float.NaN;
        this.currentLoad = 300f;
        this.stateScaling = false;
        this.cluster = "localhost:9092";
        this.numThreads = "1";
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "streamsKafkaParserSecondNode");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, cluster);
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.NUM_STREAM_THREADS_CONFIG, numThreads);
        props.put(StreamsConfig.METRICS_RECORDING_LEVEL_CONFIG, "DEBUG");
        props.put(StreamsConfig.BUFFERED_RECORDS_PER_PARTITION_CONFIG, Integer.MAX_VALUE);
        props.put(StreamsConfig.POLL_MS_CONFIG, 10);
        props.put(StreamsConfig.METRICS_SAMPLE_WINDOW_MS_CONFIG, 1000);
    }

    public KafkaParserSecondNode() {
        this.id = UUID.fromString("198633cc-7459-4227-b6af-dbada226f8b9");
        this.CK = "CK-" + id;
        this.isLeader = true;
        this.isActive = true;
        this.nbAckReceived = 0;
        this.nbAckExprected = 0;
        this.tableOfNewAddresses = new ArrayList<>();
        this.CAPACITY = 2;
        this.RATIO_DESIRED_LOAD = 0.7f;
        this.THRES_TOP = 0.8f;
        this.THRES_DOWN = 0.6f;
        this.numberCurrentInstances = 1;
        this.currentLoad = 0f;
        this.stateScaling = false;
        this.cluster = "localhost:9092";
        this.numThreads = "4";
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "streamsKafkaParserSecondNode");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, cluster);
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.NUM_STREAM_THREADS_CONFIG, numThreads);
        props.put(StreamsConfig.METRICS_RECORDING_LEVEL_CONFIG, "DEBUG");
        props.put(StreamsConfig.BUFFERED_RECORDS_PER_PARTITION_CONFIG, Integer.MAX_VALUE);
        props.put(StreamsConfig.POLL_MS_CONFIG, 10);
        props.put(StreamsConfig.METRICS_SAMPLE_WINDOW_MS_CONFIG, 1000);
    }

    public KafkaParserSecondNode(String cluster, String numThreads) {
        this.id = UUID.fromString("198633cc-7459-4227-b6af-dbada226f8b9");
        this.CK = "CK-" + id;
        this.isLeader = true;
        this.isActive = true;
        this.nbAckReceived = 0;
        this.nbAckExprected = 0;
        this.tableOfNewAddresses = new ArrayList<>();
        this.CAPACITY = 500;
        this.RATIO_DESIRED_LOAD = 0.7f;
        this.THRES_TOP = 0.8f;
        this.THRES_DOWN = 0.6f;
        this.numberCurrentInstances = 1;
        this.currentLoad = 300f;
        this.stateScaling = false;
        this.cluster = "localhost:9092";
        this.numThreads = "1";
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "streamsKafkaParserSecondNode");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, cluster);
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.NUM_STREAM_THREADS_CONFIG, numThreads);
        props.put(StreamsConfig.METRICS_RECORDING_LEVEL_CONFIG, "DEBUG");
        props.put(StreamsConfig.BUFFERED_RECORDS_PER_PARTITION_CONFIG, Integer.MAX_VALUE);
        props.put(StreamsConfig.POLL_MS_CONFIG, 10);
        props.put(StreamsConfig.METRICS_SAMPLE_WINDOW_MS_CONFIG, 1000);
    }

    public KafkaParserSecondNode(String cluster, String numThreads, UUID id) {
        this.id = id;
        this.CK = "CK-" + id;
        this.isLeader = false;
        this.isActive = false;
        this.nbAckReceived = 0;
        this.nbAckExprected = 0;
        this.tableOfNewAddresses = new ArrayList<>();
        this.CAPACITY = 500;
        this.RATIO_DESIRED_LOAD = 0.7f;
        this.THRES_TOP = 0.8f;
        this.THRES_DOWN = 0.6f;
        this.numberCurrentInstances = Float.NaN;
        this.currentLoad = 300f;
        this.stateScaling = false;
        this.cluster = "localhost:9092";
        this.numThreads = "1";
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "streamsKafkaParserSecondNode");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, cluster);
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.NUM_STREAM_THREADS_CONFIG, numThreads);
        props.put(StreamsConfig.METRICS_RECORDING_LEVEL_CONFIG, "DEBUG");
        props.put(StreamsConfig.BUFFERED_RECORDS_PER_PARTITION_CONFIG, Integer.MAX_VALUE);
        props.put(StreamsConfig.POLL_MS_CONFIG, 10);
        props.put(StreamsConfig.METRICS_SAMPLE_WINDOW_MS_CONFIG, 1000);
    }

    public static void main(String[] args) {

        ArrayList<ArrayList<String>> metricsLatenceTest = new ArrayList<>();
        Protocol protocol = new Protocol();
        KafkaParserSecondNode node2;
        if (args.length == 0) {
            node2 = new KafkaParserSecondNode();
        } else if (args.length == 1) {
            node2 = new KafkaParserSecondNode(UUID.fromString(args[0]));
        } else if (args.length == 2) {
            node2 = new KafkaParserSecondNode(args[0], args[1]);
        } else {
            node2 = new KafkaParserSecondNode(args[0], args[1], UUID.fromString(args[2]));
        }
        Repertory repertory = new Repertory();
        System.out.println(node2.isActive);
        /**
         * Producer's configuration for Kafka Stream
         */
        /**
         * Producer's configuration for debugging (CK-input/output)
         */
        Properties configProperties = new Properties();
        configProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, cluster);
        configProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        configProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        Producer prodDeploy = new KafkaProducer<>(configProperties);

        /**
         * Producer's configuration for consumer (scaling algorithm)*
         */
        Properties scalingprops = new Properties();
        scalingprops.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, cluster);
        scalingprops.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        scalingprops.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonPOJOSerializer.class.getName());
        Producer prodScaling = new KafkaProducer<>(scalingprops);

        final StreamsBuilder builder = new StreamsBuilder();
        KStream<String, String> inputData = builder.stream("input-stream-taxi");
        KStream<String, String> output = inputData.filter((key, value) -> {
            //long t1 = System.currentTimeMillis();
            Boolean bool = parseData(value);
            //long t2 = System.currentTimeMillis();
            //System.out.println("latence = " + (t2 - t1));
            return true;
        });
        output.to("output-stream-taxi");

        ScalingConsumerThread scalingMsg = new ScalingConsumerThread(repertory, node2, prodScaling, protocol, prodDeploy);
        scalingMsg.start();
//        LocalScaling scaling = new LocalScaling(repertory, node2, prodScaling, prodDeploy);
//        scaling.start();
        final Topology topology = builder.build();
        final KafkaStreams streams = new KafkaStreams(topology, props);
        final CountDownLatch latch = new CountDownLatch(1);
        MetricThreadKafka metricThreadKafka = new MetricThreadKafka(streams, node2, repertory, prodScaling);
        metricThreadKafka.start();

        // attach shutdown handler to catch control-c
        Runtime.getRuntime().addShutdownHook(new Thread("streams-shutdown-hook") {
            @Override
            public void run() {
                streams.close();
                latch.countDown();
            }
        });
        try {
            streams.start();

            latch.await();
        } catch (IllegalStateException | InterruptedException | StreamsException e) {
            System.exit(1);
        }
    }

    static Boolean parseData(String data) {
        String[] splitData = data.split(",");
//        String medallion = splitData[0];
//        String hack_license = splitData[1];
        String pickup_datetime = splitData[2];
        String dropoff_datetime = splitData[3];
        String trip_time_in_secs = splitData[4];
        String trip_distance = splitData[5];
        String pickup_longitude = splitData[6];
        String pickup_latitude = splitData[7];
        String dropoff_longitude = splitData[8];
        String dropoff_latitude = splitData[9];
//        String payment_type = splitData[10];
//        String fare_amount = splitData[11];
//        String surcharge = splitData[12];
//        String mta_tax = splitData[13];
//        String tip_amount = splitData[14];
//        String tolls_amount = splitData[15];
//        String total_amount = splitData[16];
        if (!checkDate(pickup_datetime, dropoff_datetime)) {
            //System.out.println("TIME " + data);
            return false;
        } else if (!checkCoordinates(pickup_longitude, pickup_latitude, dropoff_longitude, dropoff_latitude)) {
            //System.out.println("coord " + data);
            return false;
        } else if (!checkInsideZone(pickup_longitude, pickup_latitude, dropoff_longitude, dropoff_latitude)) {
            //System.out.println("ZONE " + data);
            return false;
        } else if (!checkTripTimeAndTripDistance(trip_time_in_secs, trip_distance)) {
            //System.out.println("trip and distance " + data);
            return false;
        } else {
            return true;
        }
    }

    static Boolean checkDate(String d1, String d2) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if (!matches(d1) || !matches(d2)) {
            return false;
        }
        Date pickupDate;
        Date dropOffDate;
        try {
            pickupDate = sdf.parse(d1);
            dropOffDate = sdf.parse(d2);
        } catch (ParseException ex) {
            return false;
        }
        return pickupDate.compareTo(dropOffDate) <= 0;
    }

    static boolean matches(String date) {
        return DATE_PATTERN.matcher(date).matches();
    }

    static Boolean checkCoordinates(String pickUpY, String pickUpX, String dropOffY, String dropOffX) {
        return !(Float.parseFloat(pickUpX) == 0.0 || Float.parseFloat(pickUpY)
                == 0.0 || Float.parseFloat(dropOffX) == 0.0 || Float.parseFloat(dropOffY) == 0.0);
    }

    static Boolean checkTripTimeAndTripDistance(String tripTime, String tripDistance) {
        return !(Float.parseFloat(tripTime) == 0.0 || Float.parseFloat(tripDistance) == 0.0);
    }

    static Boolean checkInsideZone(String pickUpY, String pickUpX, String dropOffY, String dropOffX) {
        if ((Double.parseDouble(pickUpY) > Double.parseDouble("-73.5661182")) || (Double.parseDouble(pickUpY) < Double.parseDouble("-74.913585"))
                || (Double.parseDouble(dropOffY) > Double.parseDouble("-73.5661182")) || (Double.parseDouble(dropOffY) < Double.parseDouble("-74.913585"))) {
            return false;
        }
        if ((Double.parseDouble(pickUpX) < Double.parseDouble("40.1274702")) || (Double.parseDouble(pickUpX) > Double.parseDouble("41.474937"))
                || (Double.parseDouble(dropOffX) < Double.parseDouble("40.1274702")) || (Double.parseDouble(dropOffX) > Double.parseDouble("41.474937"))) {
            return false;
        }
        return true;
    }
}
