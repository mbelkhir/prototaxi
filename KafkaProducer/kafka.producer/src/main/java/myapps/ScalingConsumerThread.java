package myapps;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.Producer;

/**
 *
 * @author mbelkhir
 */
public class ScalingConsumerThread extends Thread {

    Properties props = new Properties();
    Protocol protocol;
    Repertory repertory;
    KafkaProducerFirstNode node3;
    Producer producer;
    KafkaConsumer<String, Message> consumer;

    public ScalingConsumerThread(Repertory repertory, KafkaProducerFirstNode node3, Producer producer, Protocol protocol) {
        this.repertory = repertory;
        this.node3 = node3;
        this.producer = producer;
        this.protocol = protocol;
        props.put("bootstrap.servers", "localhost:9092");
        props.put("group.id", "consumerKafkaProducer");
        props.put("value.deserializer", "myapps.MessageDeserializer");
        props.put("enable.auto.commit", "true");
        props.put("auto.commit.interval.ms", "1000");
        props.put("auto.offset.reset", "earliest");
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Arrays.asList(node3.CK));
    }

    @Override
    public void run() {
        while (true) {
            ConsumerRecords<String, Message> records = consumer.poll(Duration.ofMillis(100));
            for (ConsumerRecord<String, Message> record : records) {
                protocol.onReceiptOf(record.value(), repertory, node3, producer);
            }
        }
    }
}
