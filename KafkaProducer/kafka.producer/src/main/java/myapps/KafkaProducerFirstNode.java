package myapps;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import org.apache.kafka.streams.StreamsBuilder;
import java.util.Properties;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;

public class KafkaProducerFirstNode {

    static String metrics = "160.00 400.00 80.00 80.00 80.00 80.00";
    static String[] metricsArrayString = metrics.split(" ");
    static ArrayList<Integer> metricsArray = new ArrayList<>();
    static ArrayList<ArrayList<Float>> metricsLatenceTest = new ArrayList<ArrayList<Float>>();
    public static final String fileName = "data";
    public UUID id = UUID.fromString("93f38f9a-e399-4873-8cd2-53b4c5157961");
    public String CK = "CK-" + id;

    public boolean isLeader = true;
    public boolean isActive = true;
    public int nbAckReceived = 0;
    public int nbAckExprected = 0;
    public List<String> tableOfNewAddresses = new ArrayList<>();

    /**
     * provisional attributes for testing (using public attributes without
     * getters and setters)
     */
    public float CAPACITY;
    public float RATIO_DESIRED_LOAD;
    public float THRES_TOP;
    public float THRES_DOWN;
    public float currentLoad;
    public float numberCurrentInstances;
    public float sendRate;
    public float sendTotal = 0F;

    public KafkaProducerFirstNode(UUID id) {
        this.id = id;
        this.CK = "CK-" + id;
        this.isLeader = false;
        this.isActive = false;
        this.nbAckReceived = 0;
        this.nbAckExprected = 0;
        this.tableOfNewAddresses = new ArrayList<>();
        this.CAPACITY = 500;
        this.RATIO_DESIRED_LOAD = 0.7f;
        this.THRES_TOP = 0.8f;
        this.THRES_DOWN = 0.6f;
        this.numberCurrentInstances = Float.NaN;
        this.currentLoad = Float.NaN;
    }

    public KafkaProducerFirstNode() {
        this.id = UUID.fromString("93f38f9a-e399-4873-8cd2-53b4c5157961");
        this.CK = "CK-93f38f9a-e399-4873-8cd2-53b4c5157961";
        this.isLeader = true;
        this.isActive = true;
        this.nbAckReceived = 0;
        this.nbAckExprected = 0;
        this.tableOfNewAddresses = new ArrayList<>();
        this.CAPACITY = 500;
        this.RATIO_DESIRED_LOAD = 0.7f;
        this.THRES_TOP = 0.8f;
        this.THRES_DOWN = 0.6f;
        this.numberCurrentInstances = 1;
        this.currentLoad = Float.NaN;
        this.sendRate = 0F;
    }

    public static void main(String[] args) {
        for (String string : metricsArrayString) {
            metricsArray.add((int) (Float.parseFloat(string)));
        }

        Protocol protocol = new Protocol();
        KafkaProducerFirstNode node3;
        if (args.length == 0) {
            node3 = new KafkaProducerFirstNode();
        } else {
            node3 = new KafkaProducerFirstNode(UUID.fromString(args[0]));
        }
        Repertory repertory = new Repertory();
        /**
         * Producer's configuration for debugging (CK-input/output)
         */
        Properties configProperties = new Properties();
        configProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        configProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        configProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        configProperties.put(ProducerConfig.ACKS_CONFIG, "1");
        configProperties.put(ProducerConfig.METRICS_SAMPLE_WINDOW_MS_CONFIG, 1000);
        Producer producer = new KafkaProducer<>(configProperties);

        /**
         * Producer's configuration for consumer (scaling algorithm)*
         */
        Properties scalingprops = new Properties();
        scalingprops.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        scalingprops.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        scalingprops.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonPOJOSerializer.class.getName());
        Producer prodScaling = new KafkaProducer<>(scalingprops);

        final StreamsBuilder builder = new StreamsBuilder();

        ScalingConsumerThread scalingMsg = new ScalingConsumerThread(repertory, node3, prodScaling, protocol);
        scalingMsg.start();
        MetricsThread metricsThread = new MetricsThread(producer, node3, repertory, prodScaling);
        metricsThread.start();
        FileInputStream file;
        BufferedReader br = null;
        int j = 0;
        float timeSinceLastChange = 0;
        int msgPerSecond = Math.round((1000F / metricsArray.get(j)));
        try {
            file = new FileInputStream(fileName);
            br = new BufferedReader(new InputStreamReader(file));
            String line = null;
            while (true) {
                line = br.readLine();
                if (line == null) {
                    br = new BufferedReader(new InputStreamReader(file));
                    line = br.readLine();
                }
                ProducerRecord<String, String> rec = new ProducerRecord<>("input-stream-taxi", line + "");
                producer.send(rec);
                try {
                    Thread.sleep(msgPerSecond);
                } catch (InterruptedException ex) {
                    Logger.getLogger(MetricsThread.class.getName()).log(Level.SEVERE, null, ex);
                }
                timeSinceLastChange += msgPerSecond;
                if (timeSinceLastChange > 300000) {
                    timeSinceLastChange = 0;
                    j++;
                    msgPerSecond = (int) (1000F / metricsArray.get(j));
                }

//            if (node3.isLeader) {
//                ArrayList<String> aux = new ArrayList<>(repertory.getSuccs());
//                for (String succ : aux) {
//                    ProducerRecord<String, Message> recMetrics = new ProducerRecord<>(succ, new Message(null, null, null, "METRICS", repertory.getSuccs().size() + "", node3.sendRate + ""));
//                    prodScaling.send(recMetrics);
//                }
//            }          
            }
        } catch (IOException e) {
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
            }
        }
    }
}
