package myapps;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

/**
 *
 * @author mbelkhir
 */
public class LocalScaling extends Thread {

    Repertory repertory;
    KafkaProducerFirstNode node3;
    Producer producer;

    public LocalScaling(Repertory repertory, KafkaProducerFirstNode node3, Producer producer) {
        this.repertory = repertory;
        this.node3 = node3;
        this.producer = producer;
    }

    private float getProbability() {

        float globalLoad = node3.currentLoad * node3.numberCurrentInstances;
        float idealLoad = node3.CAPACITY * node3.RATIO_DESIRED_LOAD;
        float nbrInstancesDiff = Math.abs(((float) globalLoad / idealLoad) - node3.numberCurrentInstances);
        return Math.abs(nbrInstancesDiff / (float) node3.numberCurrentInstances);
    }

    private int applyProba(float p) {
        return Math.random() < p ? 1 : 0;
    }

    private void autoScaling() {
        if ((node3.currentLoad / node3.CAPACITY >= node3.THRES_TOP)) {
            scalingOut();
        } else if ((node3.currentLoad / node3.CAPACITY <= node3.THRES_DOWN && !node3.isLeader)) {
            scalingIn();
        }
    }

    private void scalingOut() {
        node3.tableOfNewAddresses = new ArrayList<>();
        float p = getProbability();
        node3.nbAckReceived = 0;
        node3.nbAckExprected = repertory.getPreds().size() + repertory.getSuccs().size();
        int nbrsNewNodes = (int) p + applyProba(p - (int) p);
        if (nbrsNewNodes != 0) {
            for (int i = 0; i < nbrsNewNodes; i++) {
                UUID newId = java.util.UUID.randomUUID();
                node3.tableOfNewAddresses.add("CK-" + newId);
                try {
                    Process process = Runtime.getRuntime().exec("kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic CK-" + newId);
                    process.waitFor();
                    ProcessBuilder builder = new ProcessBuilder(
                            "java", "-cp", "/home/mbelkhir/Documents/DStream2/streams.examples/target/streams.examples-0.1.jar:/home/mbelkhir/Documents/DStream2/streams.examples/target/dependency/*", "myapps.KafkaProducerFirstNode", newId.toString());
                    builder.redirectOutput(
                            ProcessBuilder.Redirect.to(new File("/home/mbelkhir/Documents/kafka-logs/outputKafkaProducerFirstNode-" + newId.toString())));
                    builder.start();
                } catch (IOException | InterruptedException ex) {
                }
            }
            repertory.getSuccs().stream().map((succ) -> {
                ProducerRecord<String, Message> rec = new ProducerRecord<>(succ, new Message(node3.tableOfNewAddresses, null, null, "DUPLICATION",
                        succ, node3.CK));
                producer.send(rec);
                ProducerRecord<String, Message> rec0 = new ProducerRecord<>("CK-test0", new Message(node3.tableOfNewAddresses, null, null, "DUPLICATION",
                        succ, node3.CK));
                return rec0;
            }).forEachOrdered((rec0) -> {
                producer.send(rec0);
            });
            repertory.getPreds().stream().map((pred) -> {
                ProducerRecord<String, Message> rec = new ProducerRecord<>(pred, new Message(node3.tableOfNewAddresses, null, null, "DUPLICATION",
                        pred, node3.CK));
                producer.send(rec);
                ProducerRecord<String, Message> rec0 = new ProducerRecord<>("CK-test0", new Message(node3.tableOfNewAddresses, null, null, "DUPLICATION",
                        pred, node3.CK));
                return rec0;
            }).forEachOrdered((rec0) -> {
                producer.send(rec0);
            });
        }
    }

    private void scalingIn() {
        node3.nbAckReceived = 0;
        node3.nbAckExprected = repertory.getPreds().size() + repertory.getSuccs().size();
        if (applyProba(getProbability()) == 1) {
            repertory.getSuccs().stream().map((succ) -> {
                ProducerRecord<String, Message> rec = new ProducerRecord<>(succ, new Message(null, null, null, "DELETION",
                        succ, node3.CK));
                producer.send(rec);
                ProducerRecord<String, Message> rec0 = new ProducerRecord<>("CK-test0", new Message(null, null, null, "DELETION",
                        succ, node3.CK));
                return rec0;
            }).forEachOrdered((rec0) -> {
                producer.send(rec0);
            });
            repertory.getPreds().stream().map((pred) -> {
                ProducerRecord<String, Message> rec = new ProducerRecord<>(pred, new Message(null, null, null, "DELETION",
                        pred, node3.CK));
                producer.send(rec);
                ProducerRecord<String, Message> rec0 = new ProducerRecord<>("CK-test0", new Message(null, null, null, "DELETION",
                        pred, node3.CK));
                return rec0;
            }).forEachOrdered((rec0) -> {
                producer.send(rec0);
            });
        }
    }

    @Override
    public void run() {
        while (true) {
            File file = new File("/home/mbelkhir/Documents/DStream/streams.examples/workload.dat");
            BufferedReader br;
            try {
                br = new BufferedReader(new FileReader(file));
                String st;
                while ((st = br.readLine()) != null) {
                    node3.currentLoad = Float.parseFloat(st);
                }
            } catch (FileNotFoundException ex) {
            } catch (IOException ex) {
            }

            /**
             * Period of scale equals to 10 seconds
             */
            try {
                Thread.sleep(10000);
            } catch (InterruptedException ex) {
            }
            //autoScaling();
        }
    }
}
