package myapps;

import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

/**
 *
 * @author mbelkhir
 */
public class Protocol {

    public void onReceiptOf(Message msg, Repertory repertory, KafkaProducerFirstNode node3, Producer producer) {
        switch (msg.getType()) {
            case "DELETION": {
                if (repertory.getPreds().contains(msg.getSrc())) {
                    repertory.getPreds().remove(msg.getSrc());
                } else {
                    repertory.getSuccs().remove(msg.getSrc());
                }
                ProducerRecord<String, Message> rec = new ProducerRecord<>(msg.getSrc(),
                        new Message(null, null, null, "DELETION_ACK", msg.getSrc(), node3.CK));
                producer.send(rec);
                ProducerRecord<String, Message> rec0 = new ProducerRecord<>("CK-test0",
                        new Message(null, null, null, "DELETION_ACK", msg.getSrc(), node3.CK));
                producer.send(rec0);
                break;
            }
            case "DUPLICATION": {
                if (repertory.getSuccs().contains(msg.getSrc())) {
                    msg.getAddrs().forEach((newNode) -> {
                        repertory.getSuccs().add(newNode);
                    });
                } else {
                    msg.getAddrs().forEach((newNode) -> {
                        repertory.getPreds().add(newNode);
                    });
                }
                ProducerRecord<String, Message> rec = new ProducerRecord<>(msg.getSrc(),
                        new Message(null, null, null, "DUPLICATION_ACK", msg.getSrc(), node3.CK));
                producer.send(rec);
                ProducerRecord<String, Message> rec0 = new ProducerRecord<>("CK-test0",
                        new Message(null, null, null, "DUPLICATION_ACK", msg.getSrc(), node3.CK));
                producer.send(rec0);
                break;
            }

            case "DUPLICATION_ACK": {
                node3.nbAckReceived = node3.nbAckReceived + 1;
                if (node3.nbAckReceived == node3.nbAckExprected) {
                    node3.tableOfNewAddresses.stream().map((newSibling) -> {
                        ProducerRecord<String, Message> rec = new ProducerRecord<>(newSibling,
                                new Message(null, repertory.getSuccs(), repertory.getPreds(), "START", newSibling, node3.CK));
                        producer.send(rec);
                        ProducerRecord<String, Message> rec0 = new ProducerRecord<>("CK-test0",
                                new Message(null, repertory.getSuccs(), repertory.getPreds(), "START", newSibling, node3.CK));
                        return rec0;
                    }).forEachOrdered((rec0) -> {
                        producer.send(rec0);
                    });
                }
                break;
            }
            case "DELETION_ACK": {
                node3.nbAckReceived = node3.nbAckReceived + 1;
                if (node3.nbAckReceived == node3.nbAckExprected) {
                    if (!node3.isLeader) {
                        ProducerRecord<String, Message> rec = new ProducerRecord<>("CK-test0",
                                new Message(null, null, null, "DELETED-DESTROYED", node3.CK, node3.CK));
                        producer.send(rec);
                        System.exit(0);
                    }
                }
                break;
            }
            case "START": {
                node3.isActive = true;
                repertory.setPreds(msg.getPreds());
                repertory.setSuccs(msg.getSuccs());
                ProducerRecord<String, Message> rec = new ProducerRecord<>("CK-test0",
                        new Message(null, null, null, "STARTED", node3.CK, node3.CK));
                producer.send(rec);
                break;
            }
            default:
                ProducerRecord<String, Message> rec = new ProducerRecord<>("CK-test0",
                        new Message(null, null, null, "ERROR", node3.CK, node3.CK));
                producer.send(rec);
                break;
        }
    }
}
