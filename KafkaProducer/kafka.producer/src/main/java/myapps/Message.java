package myapps;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 *
 * @author mbelkhir
 */
public class Message {

    private List<String> addrs;
    private List<String> succs;
    private List<String> preds;
    private String type;
    private String dest;
    private String src;

    @JsonCreator
    public Message(@JsonProperty("addrs") List<String> addrs, @JsonProperty("succs") List<String> succs, @JsonProperty("preds") List<String> preds, @JsonProperty("type") String type, @JsonProperty("dest") String dest, @JsonProperty("src") String src) {
        this.addrs = addrs;
        this.succs = succs;
        this.preds = preds;
        this.type = type;
        this.dest = dest;
        this.src = src;
    }

    public Message(List<String> addrs, String type, String dest, String src) {
        this.addrs = addrs;
        this.type = type;
        this.dest = dest;
        this.src = src;
    }

    public Message(List<String> succs, List<String> preds, String type, String dest, String src) {
        this.succs = succs;
        this.preds = preds;
        this.type = type;
        this.dest = dest;
        this.src = src;
    }

    public Message(String type, String dest, String src) {
        this.type = type;
        this.dest = dest;
        this.src = src;
    }

    public List<String> getAddrs() {
        return addrs;
    }

    public void setAddrs(List<String> addrs) {
        this.addrs = addrs;
    }

    public List<String> getSuccs() {
        return succs;
    }

    public void setSuccs(List<String> succs) {
        this.succs = succs;
    }

    public List<String> getPreds() {
        return preds;
    }

    public void setPreds(List<String> preds) {
        this.preds = preds;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDest() {
        return dest;
    }

    public void setDest(String dest) {
        this.dest = dest;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

}
