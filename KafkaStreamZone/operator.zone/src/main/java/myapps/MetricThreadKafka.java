package myapps;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.Metric;
import org.apache.kafka.common.MetricName;
import org.apache.kafka.streams.KafkaStreams;

/**
 *
 * @author mbelkhir
 */
public class MetricThreadKafka extends Thread {

    KafkaStreams streams;
    KafkaZoneThirdNode node;
    List<List<Float>> sendRateMetricsByThread;
    List<Float> aux;
    static ArrayList<ArrayList<String>> metricsLatenceTest = new ArrayList<ArrayList<String>>();
    List<Float> arrayMetric;
    Repertory repertory;
    Producer prodScaling;

    public MetricThreadKafka(KafkaStreams streams, KafkaZoneThirdNode node, Repertory repertory, Producer prodScaling) {
        this.streams = streams;
        this.node = node;
        sendRateMetricsByThread = new ArrayList<>();
        this.repertory = repertory;
        this.prodScaling = prodScaling;
    }

    @Override
    public void run() {
        int i = 0;
        BufferedWriter writer;
        while (true) {
            aux = new ArrayList<>();
            arrayMetric = new ArrayList<>();
            Map<MetricName, ? extends Metric> metrics = streams.metrics();
            metrics.forEach((clientid, metricMap) -> {
                if (metricMap.metricName().name().contains("record-send-rate") && metricMap.metricName().toString().contains("topic=sink-stream-taxi")) {
                    String metricName = metricMap.metricName().toString();
                    String metricValue = metricMap.metricValue().toString();
                    arrayMetric.add(Float.parseFloat(metricValue));
                }
            });
            node.sendRate = getSumMetric((ArrayList<Float>) arrayMetric);
            try {
                Thread.sleep(5000);
            } catch (InterruptedException ex) {
            }
            System.out.println("send-rate-node-zone = " + node.sendRate);
           // if (node.isLeader) {
             //   ArrayList<String> aux = new ArrayList<>(repertory.getSuccs());
               // for (String succ : aux) {
                 //   ProducerRecord<String, Message> recMetrics = new ProducerRecord<>(succ, new Message(null, null, null, "METRICS", repertory.getSuccs().size() + "", node.sendRate + ""));
                   // prodScaling.send(recMetrics);
                //}
            //}
            ArrayList<String> auxTest = new ArrayList<>();
            auxTest.add(System.currentTimeMillis() + "");
            auxTest.add(node.currentLoad + "");
            auxTest.add(node.sendRate + "");
            auxTest.add(node.numberCurrentInstances + "");
            metricsLatenceTest.add(auxTest);

            if (i == 180) {
                try {
                    writer = new BufferedWriter(new FileWriter("results/KafkaOutputMetricsProdTest" + node.id.toString().substring(0, 4) + ".py"));
                    writer.write("latence = " + metricsLatenceTest);
                    writer.close();
                    System.out.println(metricsLatenceTest);
                } catch (IOException ex) {
                }
            }
            if (i == Integer.MAX_VALUE - 1) {
                i = 0;
            } else {
                i++;
            }
        }
    }

    public Float getSumMetric(ArrayList<Float> array) {
        Float result = 0F;
        for (Float f : array) {
            result = result + f;
        }
        return result;
    }
}
