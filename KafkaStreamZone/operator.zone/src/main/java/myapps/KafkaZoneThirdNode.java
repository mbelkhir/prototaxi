package myapps;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Stream;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.streams.errors.StreamsException;
import org.apache.kafka.streams.kstream.KStream;
//OpenGTS
import org.opengts.util.GeoPoint;
import org.opengts.util.GeoPolygon;

public class KafkaZoneThirdNode {

    public static Properties props = new Properties();

    public UUID id = UUID.fromString("a6b701ca-9c36-449c-bafe-de014da3e5ea");
    public String CK = "CK-" + id;
    public static String cluster;
    public String numThreads;

    public boolean isLeader = true;
    public boolean isActive = true;
    public int nbAckReceived = 0;
    public int nbAckExprected = 0;
    public List<String> tableOfNewAddresses = new ArrayList<>();

    /**
     * provisional attributes for testing (using public attributes without
     * getters and setters)
     */
    public float CAPACITY;
    public float RATIO_DESIRED_LOAD;
    public float THRES_TOP;
    public float THRES_DOWN;
    public float currentLoad;
    public float numberCurrentInstances;
    public boolean stateScaling;
    public Float sendRate = 0F;
    public float sendTotal = 0F;

    /**
     *
     * @param id
     */
    public KafkaZoneThirdNode(UUID id) {
        this.id = id;
        this.CK = "CK-" + id;
        this.isLeader = false;
        this.isActive = false;
        this.nbAckReceived = 0;
        this.nbAckExprected = 0;
        this.tableOfNewAddresses = new ArrayList<>();
        this.CAPACITY = 60;
        this.RATIO_DESIRED_LOAD = 0.7f;
        this.THRES_TOP = 0.8f;
        this.THRES_DOWN = 0.6f;
        this.numberCurrentInstances = Float.NaN;
        this.currentLoad = 300f;
        this.stateScaling = false;
        this.cluster = "localhost:9092";
        this.numThreads = "1";
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "streamsKafkaZoneThirdNode");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, cluster);
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.NUM_STREAM_THREADS_CONFIG, numThreads);
        props.put(StreamsConfig.METRICS_RECORDING_LEVEL_CONFIG, "DEBUG");
        props.put(StreamsConfig.BUFFERED_RECORDS_PER_PARTITION_CONFIG, Integer.MAX_VALUE);
        props.put(StreamsConfig.POLL_MS_CONFIG, 10);
        props.put(StreamsConfig.METRICS_SAMPLE_WINDOW_MS_CONFIG, 1000);
    }

    public KafkaZoneThirdNode() {
        this.id = UUID.fromString("a6b701ca-9c36-449c-bafe-de014da3e5ea");
        this.CK = "CK-" + id;
        this.isLeader = true;
        this.isActive = true;
        this.nbAckReceived = 0;
        this.nbAckExprected = 0;
        this.tableOfNewAddresses = new ArrayList<>();
        this.CAPACITY = 60;
        this.RATIO_DESIRED_LOAD = 0.7f;
        this.THRES_TOP = 0.8f;
        this.THRES_DOWN = 0.6f;
        this.numberCurrentInstances = 1;
        this.currentLoad = 0f;
        this.stateScaling = false;
        this.cluster = "localhost:9092";
        this.numThreads = "4";
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "streamsKafkaZoneThirdNode");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, cluster);
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.NUM_STREAM_THREADS_CONFIG, numThreads);
        props.put(StreamsConfig.METRICS_RECORDING_LEVEL_CONFIG, "DEBUG");
        props.put(StreamsConfig.BUFFERED_RECORDS_PER_PARTITION_CONFIG, Integer.MAX_VALUE);
        props.put(StreamsConfig.POLL_MS_CONFIG, 10);
        props.put(StreamsConfig.METRICS_SAMPLE_WINDOW_MS_CONFIG, 1000);
    }

    public KafkaZoneThirdNode(String cluster, String numThreads) {
        this.id = UUID.fromString("a6b701ca-9c36-449c-bafe-de014da3e5ea");
        this.CK = "CK-" + id;
        this.isLeader = true;
        this.isActive = true;
        this.nbAckReceived = 0;
        this.nbAckExprected = 0;
        this.tableOfNewAddresses = new ArrayList<>();
        this.CAPACITY = 60;
        this.RATIO_DESIRED_LOAD = 0.7f;
        this.THRES_TOP = 0.8f;
        this.THRES_DOWN = 0.6f;
        this.numberCurrentInstances = 1;
        this.currentLoad = 40f;
        this.stateScaling = false;
        this.cluster = cluster;
        this.numThreads = numThreads;
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "streamsKafkaZoneThirdNode");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, cluster);
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.NUM_STREAM_THREADS_CONFIG, numThreads);
        props.put(StreamsConfig.METRICS_RECORDING_LEVEL_CONFIG, "DEBUG");
        props.put(StreamsConfig.BUFFERED_RECORDS_PER_PARTITION_CONFIG, Integer.MAX_VALUE);
        props.put(StreamsConfig.POLL_MS_CONFIG, 10);
        props.put(StreamsConfig.METRICS_SAMPLE_WINDOW_MS_CONFIG, 1000);
    }

    public KafkaZoneThirdNode(String cluster, String numThreads, UUID id) {
        this.id = id;
        this.CK = "CK-" + id;
        this.isLeader = false;
        this.isActive = false;
        this.nbAckReceived = 0;
        this.nbAckExprected = 0;
        this.tableOfNewAddresses = new ArrayList<>();
        this.CAPACITY = 60;
        this.RATIO_DESIRED_LOAD = 0.7f;
        this.THRES_TOP = 0.8f;
        this.THRES_DOWN = 0.6f;
        this.numberCurrentInstances = Float.NaN;
        this.currentLoad = 300f;
        this.stateScaling = false;
        this.cluster = cluster;
        this.numThreads = numThreads;
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "streamsKafkaZoneThirdNode");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, cluster);
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.NUM_STREAM_THREADS_CONFIG, numThreads);
        props.put(StreamsConfig.METRICS_RECORDING_LEVEL_CONFIG, "DEBUG");
        props.put(StreamsConfig.BUFFERED_RECORDS_PER_PARTITION_CONFIG, Integer.MAX_VALUE);
        props.put(StreamsConfig.POLL_MS_CONFIG, 10);
        props.put(StreamsConfig.METRICS_SAMPLE_WINDOW_MS_CONFIG, 1000);

    }

    public static void main(String[] args) {

        ArrayList<ArrayList<String>> metricsLatenceTest = new ArrayList<>();
        Protocol protocol = new Protocol();
        KafkaZoneThirdNode node2;
        if (args.length == 0) {
            node2 = new KafkaZoneThirdNode();
        } else if (args.length == 1) {
            node2 = new KafkaZoneThirdNode(UUID.fromString(args[0]));
        } else if (args.length == 2) {
            node2 = new KafkaZoneThirdNode(args[0], args[1]);
        } else {
            node2 = new KafkaZoneThirdNode(args[0], args[1], UUID.fromString(args[2]));
        }
        Repertory repertory = new Repertory();
        System.out.println(node2.isActive);
        /**
         * Producer's configuration for Kafka Stream
         */
        /**
         * Producer's configuration for debugging (CK-input/output)
         */
        Properties configProperties = new Properties();
        configProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, cluster);
        configProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        configProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        Producer prodDeploy = new KafkaProducer<>(configProperties);

        /**
         * Producer's configuration for consumer (scaling algorithm)*
         */
        Properties scalingprops = new Properties();
        scalingprops.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, cluster);
        scalingprops.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        scalingprops.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonPOJOSerializer.class.getName());
        Producer prodScaling = new KafkaProducer<>(scalingprops);

        GeoPolygon zone = initZone();

        final StreamsBuilder builder = new StreamsBuilder();
        KStream<String, String> inputData = builder.stream("output-stream-taxi");
        KStream<String, String> output = inputData.filter((key, value) -> {
            long t1 = System.currentTimeMillis();
            Boolean bool = parseData(value, zone);
            long t2 = System.currentTimeMillis();
            // System.out.println("latence = " + (t2 - t1));
            return true;
        });
        output.to("sink-stream-taxi");

        ScalingConsumerThread scalingMsg = new ScalingConsumerThread(repertory, node2, prodScaling, protocol, prodDeploy, cluster);
        scalingMsg.start();
        LocalScaling scaling = new LocalScaling(repertory, node2, prodScaling, prodDeploy);
        scaling.start();
        final Topology topology = builder.build();
        final KafkaStreams streams = new KafkaStreams(topology, props);
        final CountDownLatch latch = new CountDownLatch(1);
        MetricThreadKafka metricThreadKafka = new MetricThreadKafka(streams, node2, repertory, prodScaling);
        metricThreadKafka.start();
//        ProducerMetricsThreads prodMetricThread = new ProducerMetricsThreads(streams, node2);
//        prodMetricThread.start();

        // attach shutdown handler to catch control-c
        Runtime.getRuntime().addShutdownHook(new Thread("streams-shutdown-hook") {
            @Override
            public void run() {
                streams.close();
                latch.countDown();
            }
        });
        try {
            streams.start();

            latch.await();
        } catch (IllegalStateException | InterruptedException | StreamsException e) {
            System.exit(1);
        }
    }

    static Boolean parseData(String data, GeoPolygon zone) {
        String[] splitData = data.split(",");
        String pickup_datetime = splitData[2];
        String dropoff_datetime = splitData[3];
        String trip_time_in_secs = splitData[4];
        String trip_distance = splitData[5];
        String pickup_longitude = splitData[6];
        String pickup_latitude = splitData[7];
        String dropoff_longitude = splitData[8];
        String dropoff_latitude = splitData[9];
        return !(!(isInZone(zone, Double.parseDouble(pickup_longitude), Double.parseDouble(pickup_latitude)))
                || !(isInZone(zone, Double.parseDouble(dropoff_longitude), Double.parseDouble(dropoff_latitude))));
    }

    static public Boolean isInZone(GeoPolygon zone, Double x, Double y) {
        GeoPoint boatPoint = new GeoPoint(y, x);
        return (zone.isPointInside(boatPoint));
    }

    static GeoPolygon initZone() {
        String fileName = "dataZone/French_Exclusive_Economic_Zone.dat";
        ArrayList<GeoPoint> gplist = new ArrayList<>();
        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            stream.forEach((String s) -> {
                String[] sep = s.split(",");
                gplist.add(new GeoPoint(Double.parseDouble(sep[1]), Double.parseDouble(sep[0])));
            });

        } catch (IOException e) {
        }
        return new GeoPolygon(gplist);
    }
}
