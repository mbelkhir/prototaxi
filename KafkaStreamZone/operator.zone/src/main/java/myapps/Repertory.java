package myapps;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author mbelkhir
 */
public class Repertory {

    private List<String> succs = new ArrayList<>();
    private List<String> preds = new ArrayList<>(Arrays.asList("CK-198633cc-7459-4227-b6af-dbada226f8b9"));

    public List<String> getSuccs() {
        return succs;
    }

    public void setSuccs(List<String> succs) {
        this.succs = new ArrayList<>(succs);
    }

    public List<String> getPreds() {
        return preds;
    }

    public void setPreds(List<String> preds) {
        this.preds = new ArrayList<>(preds);
    }
}
